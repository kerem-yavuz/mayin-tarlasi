/**
 * Mine Item Controller
 * @use <MineItem />
 * Mayın wrapper nesnesi içerisinde yer alan her bir item'ı temsil etmektedir.
 */
import props from './model';
export default {
  props: props,
  methods: {
    show(e) {
      /** Basılan mayının içeriğini göster */
      e.status = true;

      /** Mayın varsa oyunu bitir */
      if (e.isMine) {
        this.$store.dispatch('gameOver');
        /** Game over ekranını göster */
        setTimeout(() => this.$store.dispatch('gameOverModal'), 5000);
      } else {

        /** 
         * Mayın yoksa puanı arttır 
         * Bir alanın etrafında herhangi bir mayın yoksa 2 puan, değilse 5 puan eklendi
        */
        this.$store.dispatch('setScore', e.mineCount == 0 ? 2 : 5);
      }
    }
  }
}