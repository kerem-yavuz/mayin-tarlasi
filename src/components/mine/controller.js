/**
 * Gerekli View Nesneleri dahil ediliyor
 */
import MineItem from '@/components/mine-item/view';


/**
 * Mine Wrapper Controller
 * @use <Mines />
 * İçerisinde MineItem nesnelerini listeleyen ana view nesnemiz
 */

export default {
  components: {
    MineItem
  },
  methods: {
    /**
     * Grid alanının kaç parçalı olacağına göre bize bir Dizi data oluşturmakta
     * İçerisinde açık/kapalı durumu, mayın durumu veya etrafında mayın varsa kaç tane olduğu bilgisi
     */
    getNewMatrixGrid() {
      // Geri dönüş nesnesi
      let list = [];

      for (let i = 0; i < this.$store.state.grid; i++) {
        /** Bir satır içinde kaç kayıt olacaksa o kadar değer oluşturuluyor */
        let value = Array.from({
          length: this.$store.state.grid
        },
          (a, b) => b = { status: false, isMine: false, mineCount: 0 });

        /** Oluşturulan değerler listeye atılıyor */
        list.push(value);
      }
      // Return
      return list;
    },

    /**
     * 
     * Bu fonksiyonun amacı, işlem göre satırdaki kaydın
     * etrafında mayın varmı, varsa kaç tane bilgisini bizim için bulur
     * @param {Object} data Mevcut data
     * @param {Number} rowIndex Satır numarası
     * @param {Number} newRowIndex Mevcut satırdan farklı bir satıra geçilecekse, kaç adım olduğu bilgisi. +-0123...
     * @param {Number} colIndex Sütun numarası
     * @param {Number} newColIndex Mevcut sütundan farklı bir sütuna geçilecekse, kaç adım olduğu bilgisi. +-0123...
     */


    findMineAndCalc(data, rowIndex, newRowIndex, colIndex, newColIndex) {
      /**
       * İstenen satırı bul
       */
      let row = data[rowIndex + (newRowIndex)];
      /**
       * İstenen sütunu bul
       */
      let col = row && row[colIndex + (newColIndex)];

      /**Eğer satın ve sütun varsa*/
      if (row && col) {
        /**
         * Eğer bulduğunda satır ve sutuna ait data içerisinde mayın varsa,
         * mevcut gönderilen, yani işlemde olan satırın mayın sayısını 1 arttır 
         */
        data[rowIndex][colIndex].mineCount += (col && col.isMine ? 1 : 0);
      }
    },

    /**
     * İlgili noktanın etrafındaki mayınları bulur
     * @param {Object} data 
     */
    findMines(data) {
      for (let i = 0; i < this.$store.state.grid; i++) {
        for (let n = 0; n < this.$store.state.grid; n++) {
          if (!data[i][n].isMine) {
            //Sol Üst Çarpraz
            this.findMineAndCalc(data, i, -1, n, -1);
            //Sol Üst
            this.findMineAndCalc(data, i, -1, n, 0);
            //Sağ Üst Çapraz
            this.findMineAndCalc(data, i, -1, n, 1);
            // Sol 
            this.findMineAndCalc(data, i, 0, n, -1);
            // Sağ 
            this.findMineAndCalc(data, i, 0, n, 1);
            // Sol Alt Çapraz
            this.findMineAndCalc(data, i, 1, n, -1);
            // Alt 
            this.findMineAndCalc(data, i, 1, n, 0);
            // Sağ Alt Çapraz
            this.findMineAndCalc(data, i, 1, n, 1);
          }
        }
      }
      return data;
    },


    /**
     * Grid sayısı kadar rasgele mayın oluşturur
     * @param {Object} tempGridData 
     */
    getRandomMine(tempGridData) {
      let count = this.$store.state.grid;
      while (count > 0) {
        let row = Math.round(Math.random() * (this.$store.state.grid - 1));
        let col = Math.round(Math.random() * (this.$store.state.grid - 1));
        if (!tempGridData[row][col].isMine) {
          count--;
          tempGridData[row][col].isMine = true
        }
      }
      return tempGridData;
    },

    /**
     * Grid Matrix boyutunu verir 5*5 6*6 7*7 => 25, 36, 49
     */
    matrixSize() {
      return Math.pow(this.$store.state.grid, 2);
    },

    /**
     * Grid üzerinde oluşturma işlemini yapar
     */
    getData() {
      
      /** Matrix oluşturuluyor */
      let _gridData = this.getNewMatrixGrid(this.$store.state.grid);

      /** Rasgele mayınlar belirleniyor */
      _gridData = this.getRandomMine(_gridData);

      /** Etrafındaki mayınlar hesaplanıyor */
      _gridData = this.findMines(_gridData);

      /** Matrix tek sutunlu dizi haline getiriliyor */
      _gridData = [].concat.apply([], _gridData);

      /** Data güncelleniyor */
      this.$store.dispatch('changeGridData', _gridData);
    },
  },

  /** Başlangıçta oluştur */
  created() {
    /** Güncellemeyi trigger edebileceğimi fonksiyon iletiliyor */
    this.$store.dispatch('setUpdateAction', this.getData);
    /** Varsayılan data */
    this.getData();
  }
}