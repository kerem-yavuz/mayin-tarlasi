import Mines from '@/components/mine/view';
import Header from '@/components/header/view';

export default {
  components: {
    Mines,
    Header
  },
}