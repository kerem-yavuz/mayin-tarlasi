import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    grid: 5,
    gridData: [],
    updateAction: null,
    gameover: false,
    isStart: false,
    scores: [],
    user: {
      name: '',
      score: 0
    }
  },
  getters: {
    getScores(state) {
      return state.scores.sort((a, b) => b.score - a.score).filter((item, index) => index < 10);
    }
  },
  mutations: {
    changeGridSize(state, payload) {
      state.grid = payload;
      state.updateAction();
    },
    changeGridData(state, payload) {
      state.gridData = payload;
    },
    setUpdateAction(state, payload) {
      state.updateAction = payload;
    },
    gameOver(state, payload) {
      state.gridData.forEach(item => item.status = true);
    },
    gameOverModal(state) {
      state.gameover = true;
    },
    startGame(state) {
      state.isStart = true;
      state.user.score = 0;
      state.updateAction();
      state.gameover = false;
    },
    resetGame(state) {
      state.scores.push({ name: state.user.name, score: state.user.score });
      state.isStart = false;
      state.user.name = '';
      state.user.score = 0;
      state.updateAction();
      state.gameover = false;
    },
    setScore(state, payload) {
      state.user.score += payload;
    }
  },
  actions: {
    changeGridSize({ commit }, payload) {
      commit('changeGridSize', payload);
    },
    changeGridData({ commit }, payload) {
      commit('changeGridData', payload);
    },
    setUpdateAction({ commit }, payload) {
      commit('setUpdateAction', payload);
    },
    gameOver({ commit }, payload) {
      commit('gameOver', payload);
    },
    gameOverModal({ commit }) {
      commit('gameOverModal');
    },
    startGame({ commit }) {
      commit('startGame');
    },
    resetGame({ commit }) {
      commit('resetGame');
    },
    setScore({ commit }, payload) {
      commit('setScore', payload);
    }
  }
});